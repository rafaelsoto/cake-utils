<?php
function shortnum($id){
  $codeset = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  $base = strlen($codeset);
  
  $converted = "";
  while ($id > 0) {
    $converted = substr($codeset, ($id % $base), 1) . $converted;
    $id = floor($id/$base);
  }
  
  return $converted;
}

function unshortnum($converted){
  $codeset = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  $base = strlen($codeset);
  
  $c = 0;
  for ($i = strlen($converted); $i; $i--) {
    $c += strpos($codeset, substr($converted, (-1 * ( $i - strlen($converted) )),1)) * pow($base,$i-1);
  }
  return $c;
}
