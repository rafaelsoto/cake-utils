<?php
class FlashComponent extends Component{
  var $components = array('Session');
  
  function flash($params=array()){
    $mensaje = !empty($params['mensaje']) ? $params['mensaje'] : NULL;
    $element = !empty($params['element']) ? $params['element'] : NULL;
    $parameters = !empty($params['params']) ? $params['params'] : NULL;
    $key = !empty($params['key']) ? $params['key'] : 'flash';
    $this->Session->setFlash($mensaje, $element, $parameters, $key);
  }
  
  function exito($mensaje='', $params=array()){
    if(is_array($mensaje)){
      list($default['params']['titulo'], $default['mensaje']) = $mensaje;
    } else {
      $default['mensaje'] = $mensaje;
    }
    
    $default['element'] = 'flash-exito';
    $params = $default + $params;
    $this->flash($params);
  }
}
