<?php
class CadenaComponent extends Component{
  var $components = array('Access', 'Cookie');

  function trollLevel($str, $p=0.5){
    $up = strlen(preg_replace("/[^A-Z]/", '', $str))+0.01;
    $lw = strlen(preg_replace("/[^a-z]/", '', $str))+0.01;
    return ($up/($up+$lw)) > $p ? true : false;
  }
  
  function checkMinihash($tocheck, $tohash){
    $check_num = array();
    $arr = array(time(), strtotime('-1 day'));
    $user_id = $this->Access->getmy('id');
    if(empty($user_id)){
      $user_id = $_SERVER['REMOTE_ADDR'];
    }
    
    foreach($arr as $t){
      $num = substr(Security::hash(date('dmy', $t) . $tohash.$user_id), 0, 7);
      $num = shortnum(hexdec($num));
      $check_num[] = substr($num, 0, strlen($num) > 5 ? 5 : strlen($num));
    }
    return in_array($tocheck, $check_num);
  }
  
  function minihash($tohash){
    $user_id = $this->Access->getmy('id');
    if(empty($user_id)){
      $user_id = $_SERVER['REMOTE_ADDR'];
    }
    
    $num = substr(Security::hash(date('dmy') . $tohash.$user_id), 0, 7);
    $num = shortnum(hexdec($num));
    $num = substr($num, 0, strlen($num) > 5 ? 5 : strlen($num));
    return $num;
  }
}
