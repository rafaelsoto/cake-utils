<?php
class FechaComponent extends Component{
  function time(){
    return strtotime($this->mysql_time());
  }
  function mysql_time($time=NULL){
    return date("c", empty($time) ? time()+(60*60*0) : $time);
  }
  function mysql_time_diff($diff=NULL){
    return $this->mysql_time(strtotime($diff, $this->time()));
  }
}
