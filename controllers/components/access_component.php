<?php
class AccessComponent extends Component{
  var $components = array('Acl', 'Auth', 'Cookie', 'Session');
  var $uses = array('Aro');#, 'UsuarioSesion');
  var $cache;
  var $cached;
  var $user;
  
  function startup(){
    $this->user = $this->Auth->user();
  }
  
  function cache(){
    if(!empty($this->cache)) return $this->cache;
    
    $cache = Cache::read('acl/cache', 'ten days');
    if(empty($cache)){
      $cache = time();
      Cache::write('acl/cache', $cache, 'ten days');
    }
    $this->cache = $cache;
    return $cache;
  }
  
  
  function check($aco, $action='*'){
    if(empty($this->user)){
      return false;
    }
    
    if(isset($this->cached['Usuario::'. $this->user['Usuario']['id'] . '/aco:' . $aco . '/action:' . $action])){
      return $this->cached['Usuario::'.$this->user['Usuario']['id'] . '/aco:' . $aco . '/action:' . $action];
    }
    
    $cache = Cache::read(Inflector::slug('acl/' . 'Usuario::'. $this->user['Usuario']['id'] . '/aco:' . $aco . '/action:' . $action), 'one day');
    if(empty($cache)){
      $cache = $this->Acl->check('Usuario::'. $this->user['Usuario']['id'], $aco, $action) ? 'true' : 'false';
      Cache::write(Inflector::slug('acl/' . 'Usuario::'. $this->user['Usuario']['id'] . '/aco:' . $aco . '/action:' . $action), $cache, 'one day');
      $this->cached['Usuario::'. $this->user['Usuario']['id'] . '/aco:' . $aco . '/action:' . $action] = $cache;
    }
    
    return $cache=='true' ? true : false;
  }
  
  function checkHelper($aro, $aco, $action = '*'){
    if(isset($this->cached[$aro . '/aco:' . $aco . '/action:' . $action]))
      return $this->cached[$aro . '/aco:' . $aco . '/action:' . $action] == 'true' ? true : false;
    
    $cache = Cache::read(Inflector::slug('acl/' . $aro . '/aco:' . $aco . '/action:' . $action), 'one day');
    if(empty($cache)){
      App::import('Component', 'Acl');
      $Acl = new AclComponent();
      $cache = $Acl->check($aro, $aco, $action) ? 'true' : 'false';
      Cache::write(Inflector::slug('acl/' . $aro . '/aco:' . $aco . '/action:' . $action), $cache, 'one day');
      $this->cached[$aro . '/aco:' . $aco . '/action:' . $action] = $cache;
    }
    
    return $cache=='true' ? true : false;
  }
  
  function checkFull($aro, $aco, $action = '*') {
    $slug = Inflector::slug('acl' . $this->cache() . '/aro/' . $aro . '/aco/' . $aco . '/action/' . $action);
    $data = Cache::read($slug, 'ten days');
    if(empty($data)){
      $data = $this->Acl->check($aro, $aco, $action) ? 'true' : 'false';
      Cache::write($slug, $data, 'ten days');
    }
    return $data=='true' ? true : false;
  }
  
  function checkLogin(){
    $user_cookie = $this->Cookie->read('Usuario.sesion');
    if(empty($this->user) && !empty($user_cookie)){
      App::import('Model', 'UsuarioSesion');
      $this->UsuarioSesion = new UsuarioSesion();
      
      $user_id = $this->UsuarioSesion->getLogin();
      if(!empty($user_id)){
        App::import('Model', 'Usuario');
        $this->Usuario = new Usuario();
        $find = $this->Usuario->find('first', array('conditions' => array('Usuario.id' => $user_id)));
        $this->Auth->login($find);
      }
    }
  }
  
  function myusername(){
    return $this->getmy('username');
  }
  
  function getmy($what){
    if(empty($this->user['Usuario'][$what])){
      return false;
    }
    return $this->user['Usuario'][$what];;
  }
}
