<?php
class FormaHelper extends Helper{
  var $helpers = array("Form");
  
  function botonimportante($nombre, $options=array()){
    $default = array('class' => 'boton botonimportante', 'div' => false);
    if(isset($options['class'])) $options['class'] .= ' '.$default['class'];
    $options = array_merge($default, $options);
    return $this->Form->submit($nombre, $options);
  }
  
  function input($input, $options=array()){
    $default = array('class' => 'inputtext');
    if(isset($options['class'])) $options['class'] .= ' '.$default['class'];
    $options = array_merge($default, $options);
    return $this->Form->text($input, $options);
  }
  
  function textarea($textarea, $options=array()){
    $default = array('class' => 'textarea');
    if(isset($options['class'])) $options['class'] .= ' '.$default['class'];
    $options = array_merge($default, $options);
    return $this->Form->textarea($textarea, $options);
  }
  
  function labelinput($label, $input, $options=array()){
    $options_label = isset($options['label']) ? $options['label'] : array();
    $options_input = isset($options['input']) ? $options['input'] : array();
    return $this->Form->label($input, $label, $options_label).' '.$this->input($input, $options_input);
  }
  
  function labelbrinput($label, $input, $options=array()){
    $options_label = isset($options['label']) ? $options['label'] : array();
    $options_input = isset($options['input']) ? $options['input'] : array();
    return $this->Form->label($input, $label, $options_label)."<br />".$this->input($input, $options_input);
  }
  
  function labelpassword($label, $input){
    return $this->Form->label($input, $label)." ".$this->Form->password($input, array('class' => 'inputpass'));
  }
  
  function labelbrpassword($label, $input){
    return $this->Form->label($input, $label)."<br />".$this->Form->password($input, array('class' => 'inputpass'));
  }
  
  function labeltextarea($label, $input, $options=array()){
    $options_label = isset($options['label']) ? $options['label'] : array();
    $options_textarea = isset($options['textarea']) ? $options['textarea'] : array();
    return $this->Form->label($input, $label, $options_label)." ".$this->textarea($input, $options_textarea);
  }
  
  function labelbrtextarea($label, $input, $options=array()){
    $options_label = isset($options['label']) ? $options['label'] : array();
    $options_textarea = isset($options['textarea']) ? $options['textarea'] : array();
    return $this->Form->label($input, $label, $options_label)."<br />".$this->textarea($input, $options_textarea);
  }
  
  function hash($input, $tohash){
    return $this->Form->hidden($input, array('value' => Security::hash($tohash)));
  }
}
